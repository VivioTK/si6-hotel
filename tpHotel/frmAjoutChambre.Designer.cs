﻿namespace tpHotel
{
    partial class frmAjoutChambre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxEtage = new System.Windows.Forms.Label();
            this.lbxDesc = new System.Windows.Forms.Label();
            this.lbxHotel = new System.Windows.Forms.Label();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnFermer = new System.Windows.Forms.Button();
            this.numEtage = new System.Windows.Forms.NumericUpDown();
            this.tbxDesc = new System.Windows.Forms.TextBox();
            this.cbxHotel = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numEtage)).BeginInit();
            this.SuspendLayout();
            // 
            // lbxEtage
            // 
            this.lbxEtage.AutoSize = true;
            this.lbxEtage.Location = new System.Drawing.Point(110, 48);
            this.lbxEtage.Name = "lbxEtage";
            this.lbxEtage.Size = new System.Drawing.Size(41, 13);
            this.lbxEtage.TabIndex = 0;
            this.lbxEtage.Text = "Etage :";
            // 
            // lbxDesc
            // 
            this.lbxDesc.AutoSize = true;
            this.lbxDesc.Location = new System.Drawing.Point(85, 104);
            this.lbxDesc.Name = "lbxDesc";
            this.lbxDesc.Size = new System.Drawing.Size(66, 13);
            this.lbxDesc.TabIndex = 1;
            this.lbxDesc.Text = "Description :";
            // 
            // lbxHotel
            // 
            this.lbxHotel.AutoSize = true;
            this.lbxHotel.Location = new System.Drawing.Point(116, 155);
            this.lbxHotel.Name = "lbxHotel";
            this.lbxHotel.Size = new System.Drawing.Size(38, 13);
            this.lbxHotel.TabIndex = 2;
            this.lbxHotel.Text = "Hôtel :";
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Location = new System.Drawing.Point(175, 250);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(101, 38);
            this.btnEnregistrer.TabIndex = 3;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(326, 250);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(101, 38);
            this.btnAnnuler.TabIndex = 4;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnFermer
            // 
            this.btnFermer.Location = new System.Drawing.Point(471, 250);
            this.btnFermer.Name = "btnFermer";
            this.btnFermer.Size = new System.Drawing.Size(101, 38);
            this.btnFermer.TabIndex = 5;
            this.btnFermer.Text = "Fermer";
            this.btnFermer.UseVisualStyleBackColor = true;
            this.btnFermer.Click += new System.EventHandler(this.btnFermer_Click);
            // 
            // numEtage
            // 
            this.numEtage.Location = new System.Drawing.Point(190, 46);
            this.numEtage.Name = "numEtage";
            this.numEtage.Size = new System.Drawing.Size(237, 20);
            this.numEtage.TabIndex = 6;
            this.numEtage.ValueChanged += new System.EventHandler(this.numEtage_ValueChanged);
            // 
            // tbxDesc
            // 
            this.tbxDesc.Location = new System.Drawing.Point(190, 101);
            this.tbxDesc.Name = "tbxDesc";
            this.tbxDesc.Size = new System.Drawing.Size(237, 20);
            this.tbxDesc.TabIndex = 7;
            this.tbxDesc.TextChanged += new System.EventHandler(this.tbxDesc_TextChanged);
            // 
            // cbxHotel
            // 
            this.cbxHotel.FormattingEnabled = true;
            this.cbxHotel.Location = new System.Drawing.Point(190, 155);
            this.cbxHotel.Name = "cbxHotel";
            this.cbxHotel.Size = new System.Drawing.Size(237, 21);
            this.cbxHotel.TabIndex = 8;
            this.cbxHotel.SelectedIndexChanged += new System.EventHandler(this.cbxHotel_SelectedIndexChanged);
            // 
            // frmAjoutChambre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 355);
            this.Controls.Add(this.cbxHotel);
            this.Controls.Add(this.tbxDesc);
            this.Controls.Add(this.numEtage);
            this.Controls.Add(this.btnFermer);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.lbxHotel);
            this.Controls.Add(this.lbxDesc);
            this.Controls.Add(this.lbxEtage);
            this.Name = "frmAjoutChambre";
            this.Text = "frmAjoutChambre";
            this.Load += new System.EventHandler(this.frmAjoutChambre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numEtage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbxEtage;
        private System.Windows.Forms.Label lbxDesc;
        private System.Windows.Forms.Label lbxHotel;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnFermer;
        private System.Windows.Forms.NumericUpDown numEtage;
        private System.Windows.Forms.TextBox tbxDesc;
        private System.Windows.Forms.ComboBox cbxHotel;
    }
}