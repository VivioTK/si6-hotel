﻿using System;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutHotel : Form
    {
        public frmAjoutHotel()
        {
            InitializeComponent();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.raz();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            Persistance.ajouteHotel(txtNom.Text, txtAdresse.Text, txtVille.Text);
            MessageBox.Show("L'Hôtel a bien été enregistrer.", "Enregistrement Hotel", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void raz()
        {
            this.txtNom.Text = "";
            this.txtAdresse.Text = "";
            this.txtVille.Text = "";
            this.txtNom.Focus();
        }

        private void frmAjoutHotel_Load(object sender, EventArgs e)
        {

        }

        private void frmAjoutHotel_Load_1(object sender, EventArgs e)
        {

        }
    }
}
