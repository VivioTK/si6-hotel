﻿namespace tpHotel
{
    partial class Visualiser_Chambre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstChambres = new System.Windows.Forms.DataGridView();
            this.btnFermer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.lstChambres)).BeginInit();
            this.SuspendLayout();
            // 
            // lstChambres
            // 
            this.lstChambres.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lstChambres.Location = new System.Drawing.Point(45, 29);
            this.lstChambres.Name = "lstChambres";
            this.lstChambres.Size = new System.Drawing.Size(398, 251);
            this.lstChambres.TabIndex = 4;
            this.lstChambres.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.lstHotels_CellContentClick);
            // 
            // btnFermer
            // 
            this.btnFermer.Location = new System.Drawing.Point(389, 323);
            this.btnFermer.Name = "btnFermer";
            this.btnFermer.Size = new System.Drawing.Size(97, 40);
            this.btnFermer.TabIndex = 3;
            this.btnFermer.Text = "Fermer";
            this.btnFermer.UseVisualStyleBackColor = true;
            this.btnFermer.Click += new System.EventHandler(this.btnFermer_Click);
            // 
            // Visualiser_Chambre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 375);
            this.Controls.Add(this.lstChambres);
            this.Controls.Add(this.btnFermer);
            this.Name = "Visualiser_Chambre";
            this.Text = "Visualiser_Chambre";
            this.Load += new System.EventHandler(this.Visualiser_Chambre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lstChambres)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView lstChambres;
        private System.Windows.Forms.Button btnFermer;
    }
}