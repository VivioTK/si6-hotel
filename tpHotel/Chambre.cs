﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Chambre
    {
        //PROPRIETES

        private int num;
        private string nom;
        private int etage;
        private string description;
        private int codecategorie;

        //CONSTRUCTEURS

        public Chambre(int num, string nom, int etage, string description, int codecategorie)
        {
            this.num = num;
            this.nom = nom;
            this.etage = etage;
            this.description = description;
            this.codecategorie = codecategorie;
        }

        public int Num
        {
            get => num;
            set => num = value;
        }
      
        public int Etage
        {
            get => etage;
            set => etage = value;
        }
        public string Description
        {
            get => description;
            set => description = value;
        }
        public int Codecategorie
        {
            get => codecategorie;
            set => codecategorie = value;
        }
        public string Nom
        {
            get => nom;
            set => nom = value;
        }
    }
}
