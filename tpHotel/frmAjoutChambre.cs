﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbxHotel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmAjoutChambre_Load(object sender, EventArgs e)
        {
            foreach (Hotel item in Persistance.getLesHotels())
            {
                cbxHotel.Items.Add(item.GetNom);
            }
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            cbxHotel.Text = " ";
            tbxDesc.Text = " ";
            numEtage.Text = "0";

        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            foreach (Hotel item in Persistance.getLesHotels())
            {
                if (item.GetNom == cbxHotel.Text)
                {
                    Persistance.ajouterChambre((int)numEtage.Value, tbxDesc.Text, item.GetId);
                }
            }
            
        }

        private void numEtage_ValueChanged(object sender, EventArgs e)
        {

        }

        private void tbxDesc_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
