﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Hotel
    {
        //PROPRIETE

        private int id;
        private string nom;
        private string adresse;
        private string ville;

        //METHODES D'ACCES

        public int GetId
        {
            get => id; set => id = value;
        }
        public string GetNom
        {
            get => nom; set => nom = value;
        }
        public string GetAdresse
        {
            get => adresse; set => adresse = value;
        }

        public string GetVille
        {
            get => ville; set => ville = value;
        }

        //CONSTRUCTEUR

        public Hotel(int unID, string unNom, string uneAdresse, string uneVille)
        {
            id = unID;
            nom = unNom;
            adresse = uneAdresse;
            ville = uneVille;
        }

        public Hotel(string nom)
        {
            this.nom = nom;
        }
    }
}
