﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class Visualiser_Chambre : Form
    {
        public Visualiser_Chambre()
        {
            InitializeComponent();
        }

        private void lstHotels_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Visualiser_Chambre_Load(object sender, EventArgs e)
        {
            lstChambres.DataSource = Persistance.getLesChambres();
        }
    }
}
